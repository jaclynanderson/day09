public class Food
{
    private String description;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * The setter for the description member
     * 
     * @param inDescription The new description
     */
    public void setDescription ( String inDescription)
    {
        description = inDescription;
    }

    public void setDescription(String inDescription)
    {
        this.description = inDescription;
    }
    
}